class Triangulo:
    def __init__ (self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def semelhantes(self, triangulo):
        tri1 = []
        tri2 = []
        
        tri1.append(self.a)
        tri1.append(self.b)
        tri1.append(self.c)

        tri2.append(triangulo.a)
        tri2.append(triangulo.b)
        tri2.append(triangulo.c)

        tri1.sort()
        tri2.sort()

        if(tri1[0] > tri2[2]):
            if tri1[0]/tri2[0] == 2 and tri1[1]/tri2[1] == 2 and tri1[2]/tri2[2] == 2:
                return True
            return False
        if tri2[0]/tri1[0] == 2 and tri2[1]/tri1[1] == 2 and tri2[2]/tri1[2] == 2:
            return True
        if(tri1[0]==tri2[0] and tri1[1]==tri2[1] and tri1[2]==tri2[2]):
            return True
        return False

def main():
    t1 = Triangulo(2, 2, 2)
    t2 = Triangulo(4, 4, 4)
    print(t1.semelhantes(t2))
    # deve devolver True


main()